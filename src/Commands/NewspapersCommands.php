<?php

namespace Drupal\newspapers\Commands;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class NewspapersCommands extends DrushCommands {

  /**
   * Provided Newspaper Loaders keyed by type identifier.
   *
   * @var supportedTypes
   */
  private $supportedTypes = [
    'iarchives' => 'Drupal\newspapers\Newspapers\IArchivesLoader',
    'backstage' => 'Drupal\newspapers\Newspapers\BackstageLoader',
  ];

  /**
   * Command description here.
   *
   * @param string $path
   *   Argument of the directory to process.
   * @param array $options
   *   An associative array of options.
   *
   * @option type
   *   Newspaper structure: 'iarchives' (default) or 'backstage'
   * @usage newspapers:load path
   *
   * @command newspapers:load
   * @aliases np-l
   *
   * @bootstrap full
   */
  public function load(string $path, array $options = ['type' => 'iarchives']) {
    // Sanity check type, the loader can take care of the path.
    if (!array_key_exists($options['type'], $this->supportedTypes) || !class_exists($this->supportedTypes[$options['type']])) {
      $this->logger()->error(dt("The type '@type' is invalid. Choose one of the following: @types.", [
        '@type' => $options['type'],
        '@types' => implode(', ', array_keys($this->supportedTypes)),
      ]));
      return NULL;
    }
    try {
      $loader = new $this->supportedTypes[$options['type']]($path);
      $loader->load();
    }
    catch (\Exception $e) {
      $this->logger()->error(dt("The @type batch load of @path could not be completed: @error", [
        '@type' => $options['type'],
        '@path' => $path,
        '@error' => $e->getMessage(),
      ]));
    }

  }

}
