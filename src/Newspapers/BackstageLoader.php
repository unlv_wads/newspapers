<?php

namespace Drupal\newspapers\Newspapers;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\UserSession;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Loads newspapers received from Backstage.
 */
class BackstageLoader extends LoaderBase {

  const DID_PREFIX = 'rby';

  /**
   * Properties that apply to every issue using this loader.
   *
   * @var arrray
   */
  private $staticProperties = [];

  /**
   * Newspaper reference for linking issues.
   *
   * @var \Drupal\node\Entity\Node
   */
  private $newspaperTitle;

  /**
   * Constructor for the Loader Base class.
   */
  public function __construct(string $path = '') {
    parent::__construct($path);

    // Common repeating properties.
    $lookups = [
      'field_type' => [
        'vid' => 'resource_types',
        'name' => 'Text',
      ],
      'field_stand_rights' => [
        'vid' => 'standardized_rights_statement',
        'field_external_uri' => 'http://rightsstatements.org/vocab/NoC-NC/1.0/',
      ],
      'field_publisher' => [
        'vid' => 'corporate_body',
        'name' => 'University of Nevada, Las Vegas',
      ],
      'field_place_of_publication' => [
        'vid' => 'geo_location',
        'field_authority_link' => 'http://sws.geonames.org/5506956',
      ],
      'field_geographic_coverage' => [
        'vid' => 'geo_location',
        'field_authority_link' => 'http://sws.geonames.org/5506956',
      ],
    ];
    foreach ($lookups as $field => $properties) {
      $term = reset($this->termStorage->loadByProperties($properties));
      if (empty($term)) {
        throw new \Exception("Could not find term: " . print_r($properties, TRUE));
      }
      $this->staticProperties[$field] = $term->id();
    }
  }

  /**
   * Load newspapers provided by backstage.
   */
  public function load() {
    // Use a user known to have fedora permissions.
    $userid = 3;
    $account = User::load($userid);
    $accountSwitcher = \Drupal::service('account_switcher');
    $userSession = new UserSession([
      'uid'   => $account->id(),
      'name'  => $account->getAccountName(),
      'roles' => $account->getRoles(),
    ]);
    $accountSwitcher->switchTo($userSession);

    $this->logger->notice("Loading path '" . $this->path . "'...");
    foreach (glob($this->path . "/XML/*/*-METS.xml") as $mets_path) {
      preg_match('#/XML/(\w+)/(\d{8,10})#', $mets_path, $matches);
      $batch = $matches[1];
      $date_path = $matches[2];
      $did = BackstageLoader::DID_PREFIX . '_' . $date_path;

      // Issue METS used for titles, issues, and pages.
      // Use DOMDocument because SimpleXML drops the file xlink:href attribute.
      $mets_document = new \DOMDocument();
      $mets_document->load($mets_path);
      $mets_xpath = new \DOMXPath($mets_document);
      $mets_xpath->registerNameSpace('mets', 'http://www.loc.gov/METS/');

      // Load or create title.
      // The Rebel Yell metadata is a mess, so we will just create/load one for all.
      $newspaper_title = reset($this->nodeStorage->loadByProperties([
        'type' => 'newspaper_title',
        'title' => 'The Rebel Yell',
      ]));
      if (empty($newspaper_title)) {
        $newspaper_title = Node::create([
          'type' => 'newspaper_title',
          'title' => 'The Rebel Yell',
        ]);
        // Add the static properties without those that don't apply.
        foreach ($this->staticProperties as $field => $value) {
          if (in_array($field, ['field_type', 'field_stand_rights'])) {
            continue;
          }
          $newspaper_title->set($field, $value);
        }
        $newspaper_title->enforceIsNew();
        $newspaper_title->setPublished(TRUE)->save();
        $this->logger->notice("Creating Newspaper Title '@title'!", ['@title' => $newspaper_title->label()]);
      }

      // Load or create issue.
      $newspaper_issue = reset($this->nodeStorage->loadByProperties([
        'type' => 'newspaper_issue',
        'field_digital_id' => $did,
      ]));
      if (empty($newspaper_issue)) {
        // Gather the relevant metadata bits.
        // Could have grabbed the LABEL attribute for the title,
        // but we decided to add some punctuation and dates to the titles.
        // Issue title comes from the issue metadata despite being linked to 'Rebel Yell'.
        $mods_element = $mets_xpath->query('mets:dmdSec[@ID="MODSMD_ISSUE1"]/mets:mdWrap/mets:xmlData/MODS:mods')->item(0);
        $title = $mods_element->getElementsByTagName('title')->item(0)->textContent;
        if (
             $mods_element->getElementsByTagName('relatedItem')->count() > 0 && 
             $mods_element->getElementsByTagName('relatedItem')->item(0)->getElementsByTagName('identifier')->count() > 0 && 
             !empty($mods_element->getElementsByTagName('relatedItem')->item(0)->getElementsByTagName('identifier')->item(0)->textContent)
           ) {
          $volume_number = $mods_element->getElementsByTagName('relatedItem')->item(0)->getElementsByTagName('identifier')->item(0)->textContent;
          $title .= ", Vol. $volume_number";
        }
        if ($mods_element->getElementsByTagName('partNumber')->count() > 0 && !empty($mods_element->getElementsByTagName('partNumber')->item(0)->textContent)) {
          $issue_number = $mods_element->getElementsByTagName('partNumber')->item(0)->textContent;
          $title .= ", No. $issue_number";
        }
        if ($mods_element->getElementsByTagName('dateIssued')->count() > 0) {
          $issue_date = $mods_element->getElementsByTagName('dateIssued')->item(0)->textContent;
          // Make date EDTF compliant. E.g. 1976.09.22 -> 1976-09-22.
          if (strpos($issue_date, '.') !== FALSE) {
            $issue_date = str_replace('.', '-', $issue_date);
          }
          $title .= " ($issue_date)";
        }

        $newspaper_issue = Node::create(array_merge([
          'type' => 'newspaper_issue',
          'title' => $title,
          'field_digital_id' => $did,
          'field_issue_volume' => $volume_number,
          'field_issue_number' => $issue_number,
          'field_date' => $issue_date,
          'field_title' => $newspaper_title->id(),
          'field_mimetype' => 'image/jp2',
        ], $this->staticProperties));
        $newspaper_issue->enforceIsNew();
        $newspaper_issue->save();
        $this->logger->notice("Created newspaper issue '@issue' with digital ID @did.", [
          '@issue' => $newspaper_issue->label(),
          '@did' => $did,
        ]);
      }
      else {
        $this->logger->notice("@newspaper_title issue with digital ID @did already exists.", [
          '@newspaper_title' => $newspaper_title->label(),
          '@did' => $did,
        ]);
      }

      // Save the METS file to a media on issue for future reference.
      $mets_destination = 'fedora://newspapers/rby/' . $date_path . '/' . basename($mets_path);
      $mets_media = reset($this->mediaStorage->loadByProperties([
        'bundle' => 'file',
        'name' => basename($mets_path),
      ]));
      if (empty($mets_media)) {
        $mets_media = Media::create([
          'bundle' => 'file',
          'field_media_of' => [
            'target_id' => $newspaper_issue->id(),
          ],
        ]);
        $mets_media->setName(basename($mets_path))->save();
      }
      $data = file_get_contents($mets_path);
      if (\Drupal::service('file_system')->prepareDirectory(dirname($mets_destination), FileSystemInterface::CREATE_DIRECTORY) &&
        $file = file_save_data($data, $mets_destination, FileSystemInterface::EXISTS_REPLACE)) {
        $mets_media->set('field_media_file', $file->id());
        $mets_media->save();
        $this->logger->notice("Copied file: " . $file->createFileUrl());
      }

      // Add PDF service file to issue.
      $pdfs_path = $this->path . '/PDFS/' . $batch . '/';
      $pdf_filename = $date_path . '.pdf';
      $pdf_filename_new = 'rby_' . $pdf_filename;
      $media = reset($this->mediaStorage->loadByProperties([
        'bundle' => 'document',
        'name' => $pdf_filename_new,
      ]));
      if (empty($media)) {
        $media = Media::create([
          'bundle' => 'document',
          'uid' => \Drupal::currentUser()->id(),
          'field_media_of' => [
            'target_id' => $newspaper_issue->id(),
          ],
          'field_media_use' => [
            'target_id' => $this->islandoraUseTerms[LoaderInterface::ISLANDORA_USE_URI_SERVICE]->id(),
          ],
        ]);
        // We add the source file down below.
        $media->setName($pdf_filename_new)->setPublished(TRUE)->save();
        $this->logger->notice("Created Media: " . $media->label());
      }
      else {
        $this->logger->notice("Media '@file' exists.", ['@file' => $media->label()]);
      }
      $new_path = 'fedora://newspapers/rby/' . $date_path;
      $dest_path = $new_path . '/' . $pdf_filename_new;
      if (file_exists($dest_path)) {
            $this->logger->notice("File '@source' already exists at '@dest'.", [
                '@source' => $pdfs_path . $pdf_filename,
                '@dest' => $dest_path,
            ]);
            continue;
      }
      if (is_readable($pdfs_path . $pdf_filename)) {
        $data = file_get_contents($pdfs_path . $pdf_filename);
        if (\Drupal::service('file_system')->prepareDirectory($new_path, FileSystemInterface::CREATE_DIRECTORY) &&
          $file = file_save_data($data, $dest_path, FileSystemInterface::EXISTS_ERROR)) {
          $this->logger->notice("Copied file: " . $file->createFileUrl());
          $media->set('field_media_document', $file->id());
          $media->save();
        }
        else {
          $this->logger->warning("Failed to copy '@source' to '@dest'", [
            '@source' => $pdfs_path . $pdf_filename,
            '@dest' => $dest_path,
          ]);
        }
      }
      else {
        $this->logger->warning("Could not find @id file @file!", [
          '@id' => $did,
          '@file' => $pdfs_path . $pdf_filename,
        ]);
      }

      // Add pages.
      // @todo Transcript from OCR / vendor - ignore for now as the full issue will get full-text from the PDF.
      // @todo Section name - might be able to pull from logical structure... but not right now.
      foreach ($mets_xpath->query('//mets:div[@TYPE="PAGE"]') as $page_element) {
        // Page metadata.
        $weight = $page_element->getAttribute('ORDER');
        $page_label = (empty($page_element->getAttribute('LABEL'))) ? $weight : $page_element->getAttribute('LABEL');
        $page_title = $newspaper_issue->label() . ', page ' . $weight;
        $page_did = $newspaper_issue->field_digital_id->value . '_' . sprintf('%03d', $weight);

        // Grab or create page.
        $issue_page = reset($this->nodeStorage->loadByProperties([
          'type' => 'newspaper_page',
          'field_digital_id' => $page_did,
        ]));
        if (empty($issue_page)) {
          $this->logger->notice("Attempting to create newspaper issue page with digital ID @did for '@newspaper_issue'.", [
            '@newspaper_issue' => $newspaper_issue->label(),
            '@did' => $page_did,
          ]);
          $issue_page = Node::create([
            'type' => 'newspaper_page',
            'title' => $page_title,
            'field_page_sequence' => $weight,
            'field_digital_id' => $page_did,
            'field_page' => mb_substr($page_label, 0, 8),
            'field_issue' => [
              'target_id' => $newspaper_issue->id(),
            ],
          ]);
          $issue_page->save();
        }
        else {
          $this->logger->notice('Existing newspaper page "@title" (@did).', [
            '@title' => $issue_page->label(),
            '@did' => $issue_page->field_digital_id->value,
          ]);
        }

        // Page media (JP2s and ALTO XMLs).
        $page_id = $page_element->getAttribute('ID');
        foreach ($mets_xpath->query("//mets:div[@ID='$page_id']/mets:fptr/mets:par/mets:area/@FILEID") as $area) {
          $file_id = $area->value;
          $mets_provided_path = $mets_xpath->query("mets:fileSec/mets:fileGrp/mets:file[@ID='$file_id']/mets:FLocat/@xlink:href")->item(0)->value;
          $media_uses = [];
          if (stripos($file_id, 'IMG') === 0) {
            $source_path = str_replace('file://.', $this->path . '/JP2S/' . $batch, $mets_provided_path);
            $dest_path = str_replace('file://./' . $date_path, $new_path, $mets_provided_path);
            $media_uses[] = $this->islandoraUseTerms[LoaderInterface::ISLANDORA_USE_URI_ORIGINAL];
            $media_uses[] = $this->islandoraUseTerms[LoaderInterface::ISLANDORA_USE_URI_PRESERVATION];
          }
          elseif (stripos($file_id, 'ALTO') === 0) {
            $source_path = str_replace('file://.', $this->path . '/XML/' . $batch, $mets_provided_path);
            $dest_path = str_replace('file://.', $new_path, $mets_provided_path);
          }
          if (file_exists($dest_path)) {
            $this->logger->notice("File '@source' already exists at '@dest'.", [
                '@source' => $source_path,
                '@dest' => $dest_path,
            ]);
            continue;
          }
          if (is_readable($source_path)) {
            $data = file_get_contents($source_path);
            if ($file = file_save_data($data, $dest_path, FileSystemInterface::EXISTS_REPLACE)) {
              $this->logger->notice("Copied file: " . $file->createFileUrl());
              $media_label = basename($dest_path);
              $media = reset($this->mediaStorage->loadByProperties([
                'bundle' => 'file',
                'name' => $media_label,
              ]));
              if (empty($media)) {
                // @todo Need to setup on a site with a fedora filesystem.
                $media = Media::create([
                  'bundle' => 'file',
                  'name' => $media_label,
                  'uid' => \Drupal::currentUser()->id(),
                  'field_media_of' => [
                    'target_id' => $issue_page->id(),
                  ],
                ]);
                $media->setPublished(TRUE)->save();
                foreach ($media_uses as $use) {
                  $media->field_media_use->appendItem($use->id());
                }
                $this->logger->notice("Created Media: " . $media->label());
              }
              else {
                $this->logger->notice("Media '@file' exists.", ['@file' => $media->label()]);
              }
              $media->set('field_media_file', $file->id());
              $media->save();
            }
            else {
              $this->logger->warning("Failed to copy '@source' to '@dest'", [
                '@source' => $source_path,
                '@dest' => $dest_path,
              ]);
            }
          }
          else {
            $this->logger->warning("Could not find @id file @file!", [
              '@id' => $file_id,
              '@file' => $source_path,
            ]);
          }
        }
      }
    }
    $accountSwitcher->switchBack();
  }

}
