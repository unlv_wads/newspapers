<?php

namespace Drupal\newspapers\Newspapers;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Session\UserSession;
use Drupal\media\Entity\Media;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * IArchives loader.
 *
 * Supports the METS-ALTO directory structure provided by iArchives.
 */
class IArchivesLoader extends LoaderBase {

  /**
   * Maps file extensions to their PCDM Use tags.
   *
   * @var array
   */
  protected $useMap = [];

  /**
   * Constructor.
   */
  public function __construct(string $path = '') {
    parent::__construct($path);
    $this->useMap = [
      'pdf' => [
        $this->islandoraUseTerms[LoaderInterface::ISLANDORA_USE_URI_SERVICE]->id(),
      ],
      'jp2' => [
        $this->islandoraUseTerms[LoaderInterface::ISLANDORA_USE_URI_ORIGINAL]->id(),
        $this->islandoraUseTerms[LoaderInterface::ISLANDORA_USE_URI_PRESERVATION]->id(),
      ],
      'xml' => [],
    ];
    // We could have used extracted text for the ALTO xml file,
    // but we use that term for seach indexing,
    // and I don't know how the SOLR search will take to ALTO ocr.
  }

  /**
   * Load newspapers provided by iArchives.
   */
  public function load() {
    // Use a user known to have fedora permissions.
    $userid = 3;
    $account = User::load($userid);
    $accountSwitcher = \Drupal::service('account_switcher');
    $userSession = new UserSession([
      'uid'   => $account->id(),
      'name'  => $account->getAccountName(),
      'roles' => $account->getRoles(),
    ]);
    $accountSwitcher->switchTo($userSession);

    foreach (glob($this->path . "/*/*/*/*", GLOB_ONLYDIR) as $issue_path) {
      preg_match('#.*/(\d{10})#', $issue_path, $matches);
      if (count($matches) < 2) {
        $this->warning("Did not find an issue number at the end of '@path'", ['@path' => $issue_path]);
        continue;
      }
      $issue_path_id = $matches[1];
      $mets_document = new \DOMDocument();
      $mets_path = $issue_path . '/articles_' . $issue_path_id . '.xml';
      if (!is_readable($mets_path)) {
        // Attempt to fallback to issue path ID METS.
        $mets_path = $issue_path . '/' . $issue_path_id . '.xml';
        if (!is_readable($mets_path)) {
          $this->logger->warning("Could not load METS for '@issue_path'! Skipping.", ['@issue_path' => $issue_path]);
          continue;
        }
      }
      $mets_document->load($mets_path);
      $mets_xpath = new \DOMXPath($mets_document);
      $mets_xpath->registerNameSpace('mets', 'http://www.loc.gov/METS/');
      // We can't xpath from an element but we need the attribute qualifier,
      // so we'll just have to xpath each element individually.
      $newspaper_title_lccn = $mets_xpath->query('mets:dmdSec[@ID="issueModsBib"]/mets:mdWrap/mets:xmlData/mods:mods/mods:relatedItem/mods:identifier[@type="lccn"]')->item(0)->textContent;

      // Find the corresponding newspaper title or skip.
      // The Rebel Yell Special Case.
      if ($newspaper_title_lccn === '2005249103') {
        $newspaper_title = reset($this->nodeStorage->loadByProperties([
          'type' => 'newspaper_title',
          'title' => 'The Rebel Yell',
        ]));
        if (empty($newspaper_title)) {
          $this->logger->warning("Could not find 'The Rebel Yell'! Skipping.");
          continue;
        }
        // Replace the fake lccn with a project prefix.
        $newspaper_title_lccn = 'rby';
      }
      else {
        $newspaper_title = reset($this->nodeStorage->loadByProperties([
          'type' => 'newspaper_title',
          'field_lccn' => $newspaper_title_lccn,
        ]));
        if (empty($newspaper_title)) {
          $this->logger->warning("Could not find newspaper lccn @lccn for issue @issue! Skipping.", [
            '@lccn' => $newspaper_title_lccn,
            '@issue' => $issue_path_id,
          ]);
          continue;
        }
      }

      // Issue metadata.
      $volume_number = $mets_xpath->query("mets:dmdSec[@ID='issueModsBib']/mets:mdWrap/mets:xmlData/mods:mods/mods:relatedItem/mods:part/mods:detail[@type='volume']/mods:number")->item(0)->textContent;
      $issue_number = $mets_xpath->query("mets:dmdSec[@ID='issueModsBib']/mets:mdWrap/mets:xmlData/mods:mods/mods:relatedItem/mods:part/mods:detail[@type='issue']/mods:number")->item(0)->textContent;
      $edition_number = $mets_xpath->query("mets:dmdSec[@ID='issueModsBib']/mets:mdWrap/mets:xmlData/mods:mods/mods:relatedItem/mods:part/mods:detail[@type='edition']/mods:number")->item(0)->textContent;
      $edition_label = $mets_xpath->query("mets:dmdSec[@ID='issueModsBib']/mets:mdWrap/mets:xmlData/mods:mods/mods:relatedItem/mods:part/mods:detail[@type='edition']/mods:caption")->item(0)->textContent;
      $issue_date = $mets_xpath->query("mets:dmdSec[@ID='issueModsBib']/mets:mdWrap/mets:xmlData/mods:mods/mods:originInfo/mods:dateIssued")->item(0)->textContent;

      $issue_title = $newspaper_title->label();
      if (!empty($volume_number)) {
        $issue_title .= ", Vol. $volume_number";
      }
      if (!empty($issue_number)) {
        $issue_title .= ", No. $issue_number";
      }
      if (!empty($edition_label)) {
        $issue_title .= " $edition_label";
      }
      $issue_title .= " ($issue_date)";

      // Load or create issue node.
      $issue_did = $newspaper_title_lccn . '_' . $issue_path_id;
      $newspaper_issue = reset($this->nodeStorage->loadByProperties([
        'type' => 'newspaper_issue',
        'field_digital_id' => $issue_did,
      ]));
      if (empty($newspaper_issue)) {
        $newspaper_issue = Node::create([
          'type' => 'newspaper_issue',
          'title' => $issue_title,
          'field_digital_id' => $issue_did,
          'field_issue_volume' => $volume_number,
          'field_issue_number' => $issue_number,
          'field_date' => $issue_date,
          'field_title' => $newspaper_title->id(),
          'field_mimetype' => 'image/jp2',
        ]);
        $newspaper_issue->enforceIsNew();
        $newspaper_issue->save();
        $this->logger->notice("Created newspaper issue '@issue' with digital ID @did.", [
          '@issue' => $newspaper_issue->label(),
          '@did' => $issue_did,
        ]);
      }
      else {
        $this->logger->notice("@newspaper_title issue with digital ID @did already exists.", [
          '@newspaper_title' => $newspaper_title->label(),
          '@did' => $issue_did,
        ]);
      }

      // Attach issue articles METS xml.
      $dest_path = 'fedora://newspapers/' . $newspaper_title_lccn . '/' . $issue_path_id . '/';
      \Drupal::service('file_system')->prepareDirectory($dest_path, FileSystemInterface::CREATE_DIRECTORY);
      $mets_filename = basename($mets_path);
      $media = reset($this->mediaStorage->loadByProperties([
        'bundle' => 'document',
        'name' => $mets_filename,
      ]));
      if (empty($media)) {
        // @todo Need to setup on a site with a fedora filesystem.
        $media = Media::create([
          'bundle' => 'file',
          'uid' => \Drupal::currentUser()->id(),
          'field_media_of' => [
            'target_id' => $newspaper_issue->id(),
          ],
        ]);
        // We add the source file down below.
        $media->setName($mets_filename)->setPublished(TRUE)->save();
        $this->logger->notice("Created Media: " . $media->label());
      }
      else {
        $this->logger->notice("Media '@file' exists.", ['@file' => $media->label()]);
      }
      if (is_readable($mets_path) && !file_exists($dest_path . $mets_filename)) {
        $data = file_get_contents($mets_path, FileSystemInterface::CREATE_DIRECTORY);
        if ($file = file_save_data($data, $dest_path . $mets_filename, FileSystemInterface::EXISTS_ERROR)) {
          $this->logger->notice("Copied file: " . $file->createFileUrl());
          $media->set('field_media_file', $file->id());
          $media->save();
        }
        else {
          $this->logger->warning("Either skipping existing or failed to copy '@source' to '@dest'", [
            '@source' => $mets_path,
            '@dest' => $dest_path . $mets_filename,
          ]);
        }
      }
      else {
        $this->logger->warning("Could not find @id file @file, or it has already been copied.", [
          '@id' => $issue_did,
          '@file' => $mets_path,
        ]);
      }

      // Gather pages metadata.
      foreach ($mets_xpath->query('mets:structMap//mets:div[@TYPE="np:page"]') as $page) {
        // Grab the mets element for the dmd_id_attribute.
        $dmd_id = $page->getAttribute('DMDID');
        $sequence_number = $mets_xpath->query("mets:dmdSec[@ID='$dmd_id']/mets:mdWrap/mets:xmlData/mods:mods/mods:relatedItem[@type='original']/mods:identifier[@type='reel sequence number']")->item(0)->textContent;
        $page_label = $mets_xpath->query("mets:dmdSec[@ID='$dmd_id']/mets:mdWrap/mets:xmlData/mods:mods/mods:part/mods:extent[@unit='pages']/mods:start")->item(0)->textContent;
        $page_title = $newspaper_issue->label() . ', page ' . $page_label;
        $page_did = $newspaper_issue->field_digital_id->value . '_' . sprintf('%03d', $page_label);

        // Grab or create page.
        $issue_page = reset($this->nodeStorage->loadByProperties([
          'type' => 'newspaper_page',
          'field_digital_id' => $page_did,
        ]));
        if (empty($issue_page)) {
          $this->logger->notice("Attempting to create newspaper issue page with digital ID @did for '@newspaper_issue'.", [
            '@newspaper_issue' => $newspaper_issue->label(),
            '@did' => $page_did,
          ]);
          $issue_page = Node::create([
            'type' => 'newspaper_page',
            'title' => $page_title,
            'field_page_sequence' => $sequence_number,
            'field_digital_id' => $page_did,
            'field_page' => $page_label,
            'field_issue' => [
              'target_id' => $newspaper_issue->id(),
            ],
          ]);
          $issue_page->save();
        }
        else {
          $this->logger->notice('Existing newspaper page "@title" (@did).', [
            '@title' => $issue_page->label(),
            '@did' => $issue_page->field_digital_id->value,
          ]);
        }
        // Add page sections.
        if ($page->parentNode->getAttribute('TYPE') == 'np:section') {
          $section_id = $page->parentNode->getAttribute('DMDID');
          if ($section_label = $mets_xpath->query("mets:dmdSec[@ID='$section_id']/mets:mdWrap/mets:xmlData/mods:mods/mods:part/mods:detail/mods:number")->item(0)->textContent) {
            $issue_page->set('field_section', $section_label)->save();
          }
        }
        // Add page files (ALTO, jp2, and pdf).
        foreach ($this->useMap as $file_extension => $use_ids) {
          $source = $issue_path . '/' . sprintf('%04d', $sequence_number) . '.' . $file_extension;
          $dest = $dest_path . basename($source);
          if (is_readable($source) && !file_exists($dest)) {
            $data = file_get_contents($source, FileSystemInterface::CREATE_DIRECTORY);
            if ($file = file_save_data($data, $dest, FileSystemInterface::EXISTS_ERROR)) {
              $this->logger->notice("Copied file: " . $file->createFileUrl());
              $media_label = $issue_page->field_digital_id->value . ' ' . $file_extension;
              $media = reset($this->mediaStorage->loadByProperties([
                'bundle' => 'file',
                'name' => $media_label,
              ]));
              if (empty($media)) {
                // @todo Need to setup on a site with a fedora filesystem.
                $media = Media::create([
                  'bundle' => 'file',
                  'name' => $media_label,
                  'uid' => \Drupal::currentUser()->id(),
                  'field_media_of' => [
                    'target_id' => $issue_page->id(),
                  ],
                ]);
                $media->setPublished(TRUE)->save();
                foreach ($use_ids as $use_id) {
                  $media->field_media_use->appendItem($use_id);
                }
                $this->logger->notice("Created Media: " . $media->label());
              }
              else {
                $this->logger->notice("Media '@file' exists.", ['@file' => $media->label()]);
              }
              $media->set('field_media_file', $file->id())->save();
              // Updating the file changes the media name. Change it back.
              $media->setName($media_label)->save();
            }
            else {
              $this->logger->warning("Either skipping or failed to copy '@source' to '@dest'", [
                '@source' => $source,
                '@dest' => $dest,
              ]);
            }
          }
          else {
            $this->logger->warning("Could not find @file or it was already copied and we're skipping it.", [
              '@file' => $source,
            ]);
          }
        }
      }
    }
    $accountSwitcher->switchBack();
  }

}
