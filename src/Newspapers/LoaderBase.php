<?php

namespace Drupal\newspapers\Newspapers;

use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Base class for newspaper loaders.
 */
abstract class LoaderBase implements LoaderInterface {

  use LoggerChannelTrait;

  /**
   * A logger for all the loaders to use.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;
  /**
   * Node storage, for convienence.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;
  /**
   * Term storage, for convienence.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $termStorage;
  /**
   * Media storage, for convienence.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $mediaStorage;
  /**
   * Where the stuff to load is.
   *
   * @var string
   */
  protected $path;
  /**
   * Term entities to use for field_media_use later.
   *
   * @var array
   */
  protected $islandoraUseTerms = [];

  /**
   * Constructor for the Loader Base class.
   */
  public function __construct(string $path = '') {
    $this->logger = $this->getLogger('newspapers');
    $this->mediaStorage = \Drupal::entityTypeManager()->getStorage('media');
    $this->termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    $this->nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
    if (!is_dir($path) || !is_readable($path)) {
      throw new \Exception(t("The path '@path' either does not exist or is not readable!", ['@path' => $path]));
    }
    $this->path = $path;

    foreach ([
      LoaderInterface::ISLANDORA_USE_URI_SERVICE,
      LoaderInterface::ISLANDORA_USE_URI_ORIGINAL,
      LoaderInterface::ISLANDORA_USE_URI_PRESERVATION,
    ] as $uri) {
      $tid = reset($this->termStorage->loadByProperties([
        'vid' => 'islandora_media_use',
        'field_external_uri' => $uri,
      ]));
      if (empty($tid)) {
        throw new \Exception(t("Could not find Islandora term with URI '@uri'", ['@uri' => $uri]));
      }
      $this->islandoraUseTerms[$uri] = $tid;
    }
  }

}
