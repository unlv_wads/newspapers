<?php

namespace Drupal\newspapers\Newspapers;

/**
 * Newspaper loader interface.
 */
interface LoaderInterface {

  const ISLANDORA_USE_URI_SERVICE = 'http://pcdm.org/use#ServiceFile';
  const ISLANDORA_USE_URI_ORIGINAL = 'http://pcdm.org/use#OriginalFile';
  const ISLANDORA_USE_URI_PRESERVATION = 'http://pcdm.org/use#PreservationMasterFile';

  /**
   * Start loading newspapers.
   */
  public function load();

}
